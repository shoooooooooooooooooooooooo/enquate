<h2>Description</h2>
This program automatically answers the lesson questionnaires.

<h2>Reqirements</h2>
<ul>
<li>python3</li>
<li>requests</li>
<li>BeautifulSoup</li>
<h2>Usage</h2>
<ul>
<li>pip3 install -r requirements.txt</li>
<li>python3 enquete.py</li>
</ul>
